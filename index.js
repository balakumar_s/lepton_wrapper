(function(){
    var exec = require('child_process').exec,
        fs = require('fs');

    module.exports = {
        execute: function( sourceFile, generatedFile,  options, callback){
            exec( "./node_modules/lepton_wrapper/leptonBuild/lepton -memory=1024M -threadmemory=128M " + sourceFile + " " + generatedFile, function (error, stdout, stderr) {
                if (error) {
                    console.error('exec error: ' + error);
                    return callback(error, null);
                }
	
		fs.readFile(generatedFile, function(error, file_binary_data){
			if(error) return callback(error, null);
		});
            });
        },
    };
}())
