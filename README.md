Lepton Wrapper
===========
A NodeJS wrapper library for Dropbox Lepton compression tool.

Quick start
-----------
**This guide assumes you have node/npm already installed.**

First, run `npm install lepton_wrapper` for your app. Then, import it in your app:

```js
var lepton = require('lepton_wrapper');

// ...
```

After this you can run the below command to compress or decompress. You must provide a valid path for both of the files.

These functions takes a callback that returns an error object and the output in form of a binary file blob.

For Compression:

```js
let sourceFileName = "IMG_20220101_004700.jpg";
let compressedFileName = "IMG_20220101_004700.lep";

lepton.execute(sourceFileName,  compressedFileName,
{
  unjailed: true,
},
function(err, data){
  if(err) throw err;
  
  console.log(`Successfully compressed ${sourceFileName}`);
});
```

For Decompression:

```js
let sourceFileName = "IMG_20220101_004700.lep";
let decompressedFileName = "IMG_20220101_004700.jpg";

lepton.execute(sourceFileName,  decompressedFileName,
{
  unjailed: true,
},
function(err, data){
  if(err) throw err;
  
  console.log(`Successfully decompressed ${sourceFileName}`);
});
```

## Options

#### unjailed
  type: Boolean

